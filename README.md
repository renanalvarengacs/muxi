# Test - muxi

This project was a test from muxi. The purpose was to create an application that displays two different profiles (proposal capture and credit analist), and also a list of clients with their information and status.

## Prerequisites

 ```bash

 Node.js - Install Node.js and the npm package manager.

 ```


## Steps

 ```bash

 $ git clone https://github.com/renanalvarenga/muxi
 $ cd muxi
 $ npm install && npm start

 ```

## Available Scripts

In the project directory, you can run:

## Development

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.<br>

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

