import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { AlertService } from '../../services/alert.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  profile: [];
  loading: boolean;

  constructor(private userService: UserService, private alertService: AlertService, private router: Router) { }

  ngOnInit() {
    this.getProfile();
  }

  getUrl(id) {
    this.router.navigate(['clients/profile', id]);
  }

  getProfile() {
    this.loading = true;
    this.userService.getAll().subscribe(
      response => {
        this.profile = response;
        this.loading = false;
      },
      error => {
        this.alertService.openSnackBar(error.message);
        this.profile = []
        this.loading = false;
      }
    )
  }

}
