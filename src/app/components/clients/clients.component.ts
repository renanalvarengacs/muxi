import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../services/client.service';
import { AlertService } from '../../services/alert.service';
import { FormControl, FormGroup } from '@angular/forms';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent implements OnInit {
  clients: any[];
  loading: boolean;
  form: FormGroup;
  formSearch: FormGroup;
  isEdit: boolean;
  showForm: boolean;
  userId: any;
  clientId: any;

  constructor(private clientService: ClientService, private alertService: AlertService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.getClients();
    this.getForm();
    this.userId = this.route.snapshot.paramMap.get("id");

    this.formSearch = new FormGroup({
      search: new FormControl(null)
    });
  }

  getCurrencyMask(value) {
    return value.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'});
  }

  getPhoneMask(value) {
    let phone = value.match(/(\d{2})(\d{4})(\d{4})/);
    return "(" + phone[1] + ") " + phone[2] + "-" + phone[3];
  }

  getForm(values?) {
    this.isEdit = !!values;
    this.clientId = values?.id;
    this.form = new FormGroup({
      name: new FormControl(values?.name),
      email: new FormControl(values?.email),
      phone: new FormControl(values?.phone),
      proposalValue: new FormControl(values?.proposalValue),
      proposalStatus: new FormControl(values? values?.proposalStatus : 'pending')
    });
  }

  changeValue(value) {
    this.getClients(value);
  }

  onSubmit(values) {
    this.showForm = false;
    this.loading = true;

    if (this.isEdit) {
      this.clientService.update(this.clientId, values.proposalStatus, this.userId).subscribe(
        response => {
          this.alertService.openSnackBar("A proposta foi atualizada com sucesso!");
          this.getClients();
          this.loading = false;
        },
        error => {
          this.alertService.openSnackBar(error.message);
          this.loading = false;
        }
      )
    } else {
      this.clientService.create(values).subscribe(
        response => {
          this.getClients();
          this.alertService.openSnackBar("O cliente foi criado com sucesso!");
          this.loading = false;
        },
        error => {
          this.alertService.openSnackBar(error.message);
          this.loading = false;
        }
      )
    }
  }

  getFilter(client, value, type) {

    return !client[type].localeCompare(value.search, undefined, { sensitivity: 'base' })
  }

  getClients(value?) {
    this.loading = true;
    this.clientService.getAll().subscribe(
      response => {
        this.clients = response;
        this.loading = false;

        if (value) {
          this.clients = this.clients.filter(client => 
            this.getFilter(client, value, 'name') || 
            this.getFilter(client, value, 'email') ||
            this.getFilter(client, value, 'phone') || 
            this.getFilter(client, value, 'proposalStatus'));
        }
      },
      error => {
        this.alertService.openSnackBar(error.statusText);
        this.clients = []
        this.loading = false;
      }
    )
  }
}
