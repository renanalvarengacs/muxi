import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  USER_URL = 'http://localhost:8080/users';

  constructor(private http: HttpClient) {}

  getAll() {
    return this.http.get<[]>(`${this.USER_URL}`);
  } 
}
