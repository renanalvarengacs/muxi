import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  CLIENT_URL = 'http://localhost:8080/clients';

  constructor(private http: HttpClient) {}

    getAll() {
      return this.http.get<[]>(`${this.CLIENT_URL}`);
    }

    create(client) {
      return this.http.post<[]>(`${this.CLIENT_URL}`, client);
    }  

    update(id, status, userId) {
      return this.http.put<[]>(`${this.CLIENT_URL}/${id}?status=${status}&userId=${userId}`, {});
    }  
}
